﻿using AdivinaNumero.Filtros;
using System.Web;
using System.Web.Mvc;

namespace AdivinaNumero
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}
	}
}
