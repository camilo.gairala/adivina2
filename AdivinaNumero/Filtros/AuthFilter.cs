﻿using AdivinaNumero.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AdivinaNumero.Filtros
{
    public class AuthFilter : ActionFilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var partida = (PartidaActual)filterContext.HttpContext.Session["partida"];
            if (partida == null || partida.jugadorActual == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary { { "Controller", "Home" }, { "Action", "Index" } }
                );
            }
            
        }
    }
}