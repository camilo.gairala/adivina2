﻿using AdivinaNumero.Filtros;
using AdivinaNumero.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdivinaNumero.Controllers
{
    public class HomeController : Controller
	{
        public ActionResult Index()
        {

            return View("Inicio");

        }
        public ActionResult Inicio()
		{
			return View();
		}

        [AuthFilter]
        public ActionResult NuevaPartida()
        {
            var partidaActual = (PartidaActual)Session["partida"];
            partidaActual.Reiniciar();
            return RedirectToAction("Jugar");
        }

        [HttpPost]
		public ActionResult Login(string nombre)
		{
			Jugador jugador = PuntuacionGlobal.Instance.GetJugador(nombre);
			var partidaActual = new PartidaActual(jugador);

            partidaActual.jugadorActual.PartidasGanadasSesion = 0;
            //Se que esto es bastante sucio pero si no me estaba dando problemas

			Session["partida"] = partidaActual;

			return RedirectToAction("Jugar");
		}

        [AuthFilter]
        public ActionResult Jugar()
		{
			var partidaActual = (PartidaActual)Session["partida"];
			ViewBag.Nombre = partidaActual.jugadorActual.Nombre;
			ViewBag.Numero = partidaActual.Numero;
			ViewBag.IntentosRestantes = 10 - partidaActual.Intentos;
            ViewBag.NumerosIntentados = string.Join(", ", partidaActual.ListaIntentos.ToArray());
			return View();
		}

        [AuthFilter]
        public ActionResult Salir()
        {
			Session.Abandon();
            return RedirectToAction("Index");
        }

        [AuthFilter]
        public ActionResult Intento(int elegido)
		{
            if (elegido == null)
            {
                TempData["MensajeError"] = "El valor es vacío";
                return RedirectToAction("NuevaPartida");
            }

            if(elegido < 1 || elegido > 100)
            {
                TempData["MensajeError"] = "El número no está en el rango 1-100";
                return RedirectToAction("Jugar");
            }

            var partidaActual = (PartidaActual)Session["partida"];

            if (partidaActual.ListaIntentos.Contains(elegido))
            {
                TempData["MensajeError"] = "El valor está repetido";

                return RedirectToAction("Jugar");
            }


            partidaActual.ListaIntentos.Add(elegido);
            partidaActual.Intentos = partidaActual.Intentos + 1;
			
			if (elegido == partidaActual.Numero)
			{
                string mensaje = "Usted ha ganado con " + partidaActual.Intentos + " intentos";
                ViewBag.Mensaje = mensaje;
				var puntuaciones = new ClasificacionController();
                puntuaciones.AgregarPuntuación(partidaActual);
                return View("Ganador");
            }
			else
			{
				if (partidaActual.Intentos < 10)
				{
                    if (elegido < partidaActual.Numero)
                    {
                        TempData["MenorOMayor"] = "El numero a adivinar es mayor al ingresado";
                    }
                    else
                    {
                        TempData["MenorOMayor"] = "El numero a adivinar es menor al ingresado";
                    }
					return RedirectToAction("Jugar");
				}
				else
				{
                    string mensaje = "Lamentablemente has fallado. El numero era " + partidaActual.Numero;
                    ViewBag.Mensaje = mensaje;
                    return View("Perdedor");
				}
			}
		}

        [AuthFilter]
        public ActionResult Puntuaciones()
		{
			var partidaActual = (PartidaActual)Session["partida"];

			var puntuaciones = new ClasificacionController();

			var clasificacion = puntuaciones.CargarPuntajes(partidaActual);

			/*ViewBag.Nombre1 = clasificacion.ActualSesion.Nombre;
			ViewBag.Nombre2 = clasificacion.ActualNavegador.Nombre;
			ViewBag.Nombre3 = clasificacion.MejorNavegador.Nombre;

			ViewBag.Puntos1 = clasificacion.ActualSesion.PartidasGanadas;
			ViewBag.Puntos2 = clasificacion.ActualNavegador.PartidasGanadas;
			ViewBag.Puntos3 = clasificacion.MejorNavegador.PartidasGanadas;*/

			return View(clasificacion);
        }
	}
}