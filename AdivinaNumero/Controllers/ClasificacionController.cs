﻿using AdivinaNumero.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Linq;

namespace AdivinaNumero.Controllers
{
    public class ClasificacionController
    {
        public void AgregarPuntuación(PartidaActual partida)
        {
            CargarPuntuacionSesion(partida);
			CargarPuntuacionNavegador(partida);
            CargarPuntuacionGlobal(partida);
        }

        private void CargarPuntuacionGlobal(PartidaActual partida)
        {
            PuntuacionGlobal.Instance.ActualizarPuntuaciones(partida.jugadorActual);
        }

        private void CargarPuntuacionSesion(PartidaActual partida)
        {
			partida.jugadorActual.PartidasGanadasSesion++;
        }

		private void CargarPuntuacionNavegador (PartidaActual partida)
		{
			var lista = GetPuntajesNavegador();
			if (lista.ContainsKey(partida.jugadorActual.Nombre))
			{
				lista[partida.jugadorActual.Nombre]++;
			}
			else
			{
				lista.Add(partida.jugadorActual.Nombre, 1);
			}
			SetPuntajesNavegador(lista);
			return;

		}

		private void SetPuntajesNavegador(Dictionary<string, int> nuevosPuntajes)
		{
			JavaScriptSerializer js = new JavaScriptSerializer();
			string str = js.Serialize(nuevosPuntajes);

			HttpCookie puntajesCookie = new HttpCookie("Clasificacion");
            puntajesCookie.Expires = DateTime.Now.AddMonths(2);
			puntajesCookie.Value = str;

			System.Web.HttpContext currentContext = System.Web.HttpContext.Current;

			if (System.Web.HttpContext.Current.Request.Cookies["Clasificacion"] != null)
			{
				System.Web.HttpContext.Current.Response.Cookies.Remove("Clasificacion");
			}
			System.Web.HttpContext.Current.Response.Cookies.Add(puntajesCookie);
		}

		private Dictionary<string, int> GetPuntajesNavegador()
		{
			System.Web.HttpContext currentContext = System.Web.HttpContext.Current;

			if (System.Web.HttpContext.Current.Request.Cookies["Clasificacion"] != null)
			{
				JavaScriptSerializer js = new JavaScriptSerializer();
				var puntajes = System.Web.HttpContext.Current.Request.Cookies["Clasificacion"].Value;
				Dictionary<string, int> puntajesJugadores = js.Deserialize<Dictionary<string, int>>(puntajes);
				return puntajesJugadores;
			}
			else
			{
				return new Dictionary<string, int>();

			}
		}

		public Puntuaciones CargarPuntajes(PartidaActual partida)
		{
            var puntajes = new Puntuaciones();

            puntajes.ActualSesion = partida.jugadorActual;
			puntajes.ActualNavegador = new Jugador(partida.jugadorActual.Nombre);
            puntajes.ActualNavegador.PartidasGanadas= MejorActualNavegador(partida);
            puntajes.MejorNavegador = MejorNavegador();
            puntajes.MejorEquipo = PuntuacionGlobal.Instance.GetMejor();


            return puntajes;
		}

		private Jugador MejorNavegador()
		{
            var lista = GetPuntajesNavegador();
            Jugador mejor = new Jugador();
            
            foreach(KeyValuePair<string, int> j in lista)
            {
                if(j.Value > mejor.PartidasGanadas)
                {
                    mejor.PartidasGanadas = j.Value;
                    mejor.Nombre = j.Key;
                }
            }
            return mejor;
        }


        private int MejorActualNavegador(PartidaActual partida)
        {
            var lista = GetPuntajesNavegador();
            if (lista.ContainsKey(partida.jugadorActual.Nombre))
            {
                return lista[partida.jugadorActual.Nombre];
            }
            else
            {
                return 0;
            }
        }

	}
}