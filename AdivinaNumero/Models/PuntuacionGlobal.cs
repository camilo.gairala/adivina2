﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdivinaNumero.Models
{
	public class PuntuacionGlobal
	{
        public Dictionary<string,Jugador> ListaJugadores { get; set; }
		private static PuntuacionGlobal _instance;

        private PuntuacionGlobal()
        {
            ListaJugadores = new Dictionary<string, Jugador>();
        }

		public static PuntuacionGlobal Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new PuntuacionGlobal();
				}
				return _instance;
			}
		}

        public void ActualizarPuntuaciones(Jugador jugadorActual)
        {
            ListaJugadores[jugadorActual.Nombre].PartidasGanadas++;
         
        }

		public Jugador GetJugador(string nombre)
		{
			if (ListaJugadores.ContainsKey(nombre))
				return ListaJugadores[nombre];

			var jugador = new Jugador(nombre);
			ListaJugadores.Add(nombre, jugador);
			return jugador;
		}

        public Dictionary<string,Jugador> getPuntuacionesGlobales()
        {
            return ListaJugadores;
        }

        internal Jugador GetMejor()
        {
            Jugador mejor = new Jugador();

            foreach (KeyValuePair<string, Jugador> j in ListaJugadores)
            {
                if (j.Value.PartidasGanadas > mejor.PartidasGanadas)
                {
                    mejor.PartidasGanadas = j.Value.PartidasGanadas;
                    mejor.Nombre = j.Value.Nombre;
                }
            }
            return mejor;
        }
    }
}