﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdivinaNumero.Models
{
    public class Puntuaciones
    {
        public Jugador ActualSesion { get; set; }
        public Jugador ActualNavegador { get; set; }
        public Jugador MejorNavegador { get; set; }
        public Jugador MejorEquipo { get; set; }

    }

}