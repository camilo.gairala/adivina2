﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdivinaNumero.Models
{
	public class Jugador
	{
		public string Nombre { get; set; }
		public int PartidasGanadas { get; set; }
        public int PartidasGanadasSesion { get; set; }

        public Jugador(string nombre)
		{
			Nombre = nombre;
			PartidasGanadas = 0;
		}
        public Jugador()
        {
			Nombre = "nadie";
            PartidasGanadas = 0;
        }
    }
}