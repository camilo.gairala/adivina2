﻿using AdivinaNumero.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdivinaNumero.Models
{
    public class PartidaActual
    {
        public Jugador jugadorActual { get; set; }
        public int Numero { get; set; }
        public int Intentos { get; set; }
        public List<int> ListaIntentos { get; set; }

        public PartidaActual(Jugador jugador)
        {
            this.jugadorActual = jugador;
            this.Numero = GenerarRandom();
			this.ListaIntentos = new List<int>();
			this.Intentos = 0;
        }

        public void Reiniciar()
        {
            this.Numero = GenerarRandom();
            this.ListaIntentos = new List<int>();
            this.Intentos = 0;
        }

		public int GenerarRandom()
		{
			var random = new Random();
			var i = random.Next(1, 101);
			return i;
		}

	}
}